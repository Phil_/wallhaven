#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <QLabel>
#include <QPushButton>
#include <QCommandLinkButton>
#include <QTableView>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    // menu button(s)
    // void search();

    void showImage(std::string path);

    // ui buttons
    // void setBackground();
    // void next();
    // void prev();

    // not sure if I need this
    // void showInfo();

private:
    Ui::MainWindow *ui;

    QLabel* thumbPreview;
    QPushButton *nextButton;
    QPushButton *prevButton;
    QCommandLinkButton *setImageButton;
    QTableView *infoTable;
};

#endif // MAINWINDOW_H
