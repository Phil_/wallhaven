#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    showImage("/home/maelstroem/Images/wallhaven-734918.jpg");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showImage(std::string path) {
    QPixmap pic(path.c_str());
    ui->thumbPreview->setPixmap(pic);
    ui->thumbPreview->setScaledContents(true);
}
